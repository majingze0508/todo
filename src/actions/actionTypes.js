export default {
  CREATE_TASK: 'CREATE_TASK',
  GET_ALL_TASK: 'GET_ALL_TASK',
  GET_TASK: 'GET_TASK',
  DELETE_TASK: 'DELETE_TASK',
  UPDATE_TASK: 'UPDATE_TASK'
}