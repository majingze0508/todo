import actionTypes from './actionTypes'

const createTask = (task) => {
  return fetch("/api/task/", {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(task)
  })
  .then((response) => {
    return {
      type: actionTypes.CREATE_TASK,
      payload: response.json()
    }
  });
}

const getTasks = () => {
  return fetch("/api/task/", {
    method: 'GET',
    headers: {
      "Content-Type": "application/json",
    }
  })
  .then((response) => {
    return {
      type: actionTypes.GET_ALL_TASK,
      payload: response.json()
    }
  });
}

const getTask = (id) => {
  const url = `/api/task/taskid/${id}`;
  return fetch(url, {
    method: 'GET',
    headers: {
      "Content-Type": "application/json",
    }
  })
  .then((response) => {
    return {
      type: actionTypes.GET_TASK,
      payload: response.json()
    }
  });
}

const deleteTask = (id) => {
  return fetch(`/api/task/${id}`, {
    method: 'DELETE',
    headers: {
      "Content-Type": "application/json",
    }
  })
  .then((response) => {
    return {
      type: actionTypes.DELETE_TASK,
      payload: response.json()
    }
  });
}

const updateTask = (task) => {
  return fetch(`/api/task/taskid/${task.id}`, {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(task)
  })
  .then((response) => {
    return {
      type: actionTypes.UPDATE_TASK,
      payload: response.json()
    }
  });
}

export default {
  createTask,
  getTasks,
  getTask,
  deleteTask,
  updateTask
}