import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ModalMessageBox from './ModalMessageBox';

class TaskDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false
    };
  }

  componentDidMount() {
    const location = this.props.location.pathname.split('/');
    const id = location[3];
    this.props.getTask(id);
  }

  componentDidUpdate(preProps, preState) {
    if (preProps.todos !== this.props.todos && this.props.todos.length > 0) {
      this.setState({
        id: this.props.todos[0].data.id,
        name: this.props.todos[0].data.name,
        creationTime: this.props.todos[0].data.creationTime,
        nickname: this.props.todos[0].data.nickname.data.name
      });
    }
  }

  submit(e) {
    e.preventDefault();
    const data = {
      id: this.state.id,
      name: this.state.name,
      creationTime: this.state.creationTime,
      nickname: this.state.nickname
    }
    this.props.updateTask(data);
    this.setState({
      showModal: true
    })
  }
  _onChange(e) {
    e.preventDefault();
    let value = {};
    value[e.target.id] = e.target.value;
    this.setState(value);
  }

  render(){
    return(
      <div className="task-detail col-6">
        <ModalMessageBox
          title="update succed"
          content="update a task"
          showModal={this.state.showModal}
        />
        <form onSubmit={(e) => this.submit(e)}>
          <div className="form-group">
            <label htmlFor="id">id: </label>
            <input type="text" className="form-control" id="id" placeholder="id" value={this.state.id} onChange={(e) => this._onChange(e)} />
          </div>
          <div className="form-group">
            <label htmlFor="name">name: </label>
            <input type="text" className="form-control" id="name" placeholder="name" value={this.state.name} onChange={(e) => this._onChange(e)} />
          </div>
          <div className="form-group">
            <label htmlFor="creationTime">creationTime: </label>
            <input type="text" className="form-control" id="creationTime" placeholder="yyyy-mm-dd" value={this.state.creationTime} disabled />
          </div>
          <div className="form-group">
            <label htmlFor="nickname">nickname: </label>
            <input type="text" className="form-control" id="nickname" placeholder="nickname" value={this.state.nickname} onChange={(e) => this._onChange(e)} />
          </div>
          <div className='button-group'>
            <button type="submit" className="btn btn-primary">Update</button>
            <Link to="/">
              <button type="button" className="btn btn-primary">Show List</button>
            </Link>
          </div>
        </form>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return { todos: state.todos };
}

export default connect(mapStateToProps)(TaskDetail);

