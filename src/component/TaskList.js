import React, { Component } from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import TaskDetail from './TaskDetail';

class TaskList extends Component {
  constructor(props) {
    super(props);
  }

  getTodos() {
    if (this.props.todos && this.props.todos.length > 0) {
      let dataList = [];
      if (Array.isArray(this.props.todos[0].data)) {
        dataList = this.props.todos[0].data;
      } else {
        this.props.todos.map((todo) => {
          dataList.push(todo.data);
        })
      }
      return dataList.map((todo) => {
        const url = `/detail/task/${todo.id}`;
        this._id = todo.id;
        return (
          <tr>
            <th scope="row">{todo.id}</th>
            <td>{todo.name}</td>
            <td>
              <Link to={url}>
                <button type="button" className="btn btn-primary">Detail</button>
              </Link>
              <button type="button" className="btn btn-danger" onClick={() => this.props.deleteTask(todo.id)}>Delete</button>
            </td>
          </tr>
        )
      })
    }
    return null;
  }
  render() {
    let display = `row task-list none`;
    if (this.props.display) {
      display = `row task-list display`;
    }
    return(
      <div className= {display}>
        <table className="table col-10 table-bordered">
          <thead className="thead-light">
            <tr>
              <th scope="col">id</th>
              <th scope="col">Task</th>
              <th scope="col">Operations</th>
            </tr>
          </thead>
          <tbody>
            {this.getTodos()}
          </tbody>
        </table>
      </div>
    )
  }
}
export default TaskList;