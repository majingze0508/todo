import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ModalMessageBox from './ModalMessageBox';

class NewTask extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false
    };
  }

  submit(e) {
    e.preventDefault();
    this.props.createTask(this.state);
    this.setState({
      showModal: true
    })
  }
  _onChange(e) {
    e.preventDefault();
    let value = {};
    value[e.target.id] = e.target.value;
    const date = new Date();
    value.creationTime = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`;
    this.setState(value);
  }

  render(){
    return(
      <div className="new-task-detail col-6">
        <ModalMessageBox
          title="create succed"
          content="create a new task"
          showModal={this.state.showModal}
        />
        <form onSubmit={(e) => this.submit(e)}>
          <div className="form-group">
            <input type="text" className="form-control" id="id" placeholder="id" onChange={(e) => this._onChange(e)} />
          </div>
          <div className="form-group">
            <input type="text" className="form-control" id="name" placeholder="name" onChange={(e) => this._onChange(e)} />
          </div>
          <div className="form-group">
            <input type="text" className="form-control" id="nickname" placeholder="nickname" onChange={(e) => this._onChange(e)} />
          </div>
          <div className='button-group'>
            <button type="submit" className="btn btn-primary">Create</button>
            <Link to="/">
              <button type="button" className="btn btn-primary">Show List</button>
            </Link>
          </div>
        </form>
      </div>
    )
  }
}

export default NewTask;