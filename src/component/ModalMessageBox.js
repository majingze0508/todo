import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ModalMessageBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false
    };
  }

  componentDidUpdate(preProps, preState) {
    if ((this.props.showModal !== preProps.showModal) && 
      (this.props.showModal !== this.state.showModal)) {
      this.setState({
        showModal: this.props.showModal
      });
    }
  }

  toggle() {
    this.setState({
      showModal: !this.state.showModal
    });
  }
  
  render(){
    return(
      <Modal isOpen={this.state.showModal} toggle={() => this.toggle()}>
        <ModalHeader toggle={() => this.toggle()}>{this.props.title}</ModalHeader>
        <ModalBody>
          {this.props.content}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={() => this.toggle()}>Cancel</Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default ModalMessageBox;
