import React, { Component } from  'react';
import TaskList from './TaskList';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class TaskListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayList: false,
      displayListTxt: 'Show Task List',
    }
  }

  componentDidUpdate(prePros, preState) {
    if (preState !== this.state && this.state.displayList) {
      this.props.getTasks();
    }
  }
  displayList(e) {
    e.preventDefault();
    let text;
    if (this.state.displayListTxt === 'Show Task List') {
      text = 'Hide Task List';
    } else {
      text = 'Show Task List';
    }
    this.setState({
      displayList: !this.state.displayList,
      displayListTxt: text,
    });
  }
  render() {
    return (
      <div className="task-list-container">
        <div className="button-group">
          <Link to='/new/task/'>
            <button type="button" className="btn btn-primary add-btn" >Add Task</button>
          </Link>
          <button type="button" className="btn btn-primary list-btn" onClick={(e) => this.displayList(e)}>{this.state.displayListTxt}</button> 
        </div>
        <TaskList
          display = {this.state.displayList}
          todos={this.props.todos}
          getTask={(id) => {this.props.getTask(id)}}
          createTask={(data) => {this.props.createTask(data)}}
          deleteTask={(id) => {this.props.deleteTask(id)}}
          updateTask={(task) => {this.props.updateTask(task)}}
        />
      </div>
    )
  }
}

function mapStateToProps (state) {
  return { todos: state.todos };
}

export default connect(mapStateToProps)(TaskListContainer);