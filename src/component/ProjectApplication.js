import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { Route, Switch, Redirect } from 'react-router-dom';
import { browserHistory } from 'react-router';
import Actions from '../actions/todoActions';
import TaskListContainer from './TaskListContainer';
import TaskDetail from './TaskDetail';
import NewTask from './NewTask';

class ProjectApplication extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <div id="todo-project-application" calssName="container">
        <Switch>
          <Route
            exact path="/"
            render={() => (
              <TaskListContainer
                {...this.props}
              />
            )}
          />
          <Route
            path="/new/task/"
            render={() => (
              <NewTask
                {...this.props}
              />
            )}
          />
          <Route
            path="/detail/task/:id"
            render={() => (
              <TaskDetail
                {...this.props}
              />
            )}
          />
        </Switch>
      </div>
    )
  }

}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getTasks: Actions.getTasks,
    createTask: Actions.createTask,
    getTask: Actions.getTask,
    deleteTask: Actions.deleteTask,
    updateTask: Actions.updateTask
   }, dispatch);
}

function mapStateToProps (state) {
  return { todos: state.todos };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectApplication);
