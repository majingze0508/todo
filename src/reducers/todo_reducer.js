import actionTypes from '../actions/actionTypes';

export default (state = [], action) => {
  switch (action.type) {
    case actionTypes.CREATE_TASK:
      return [action.payload, ...state];
    case actionTypes.GET_ALL_TASK:
      return [action.payload, ...state];
    case actionTypes.GET_TASK:
      return [action.payload, ...state];
    case actionTypes.DELETE_TASK:
      return [action.payload, ...state];
    case actionTypes.UPDATE_TASK:
      return [action.payload, ...state];
    default:
      return state;
  }
}