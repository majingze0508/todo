import { combineReducers } from "redux";
import TaskReducer from "./todo_reducer";

const rootReducer = combineReducers({
  todos: TaskReducer
});

export default rootReducer;
