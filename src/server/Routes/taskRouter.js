const express = require('express');
const taskRouter = express.Router();
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const tasks = require('../Data/data.json');
const path = require('path');


//const newDB = new MongoDB(config);
/* For update a task */
taskRouter.post('/taskid/:taskId', (req, res) => {
  return new Promise((resolve, reject) => {
    if (!req.body) {
      return reject(res.status(400).send('there is no form data when update a task'));
    }
    if (!req.params) {
      return reject(res.status(400).send('there is no params data when update a task'));
    }

    const data = req.body;

    data.nickname = {
      "data":{
        "name": data.nickname
      }
    };

    const id = req.params.taskId;

    const taskList =  tasks.filter((item) => item.id === id);

    if (taskList.length > 0) {
      tasks[tasks.indexOf(taskList[0])] = data;
      return fs.writeFileAsync(path.join(__dirname, '../Data/', 'data.json'), JSON.stringify(tasks), 'utf8')
        .then(() => {
          resolve(res.json({status: 200, data: taskList[0], message: "update task is succeed"}));
        })
        .catch((e) => {
          reject(res.status(400).json({status: 400, message: 'update a task failed'}));
        })
    }
    return reject(res.status(400).json({status: 400, message: "No item to delete"}));
  });
});

/* For create a task */
taskRouter.post('/', (req, res) => {
  return new Promise((resolve, reject) => {
    if (!req.body) {
      return reject(res.status(400).send('there is no form data when create a task'));
    }
    const data = req.body;
  
    data.nickname = {
      "data":{
        "name": data.nickname
      }
    };
    tasks.push(data);
  
    return fs.writeFileAsync(path.join(__dirname, '../Data/', 'data.json'), JSON.stringify(tasks), 'utf8')
      .then(() => {
        resolve(res.json({status: 200, data: tasks, message: "create task is succeed"}));
      })
      .catch((e) => {
        reject(res.status(400).json({status: 400, message: 'create a task failed'}));
      });
  }) 
});

/* For delete a task */
taskRouter.delete('/:taskId', (req, res) => {
  return new Promise((resolve, reject) => {
    if (!req.params) {
      return reject(res.status(400).send('there is no params data when delete a task'));
    }
    const id = req.params.taskId;

    const taskList =  tasks.filter((item) => item.id === id);

    if (taskList.length > 0) {
      tasks.splice(tasks.indexOf(taskList[0]), 1);
      return fs.writeFileAsync(path.join(__dirname, '../Data/', 'data.json'), JSON.stringify(tasks), 'utf8')
        .then(() => {
          resolve(res.json({status: 200, data: tasks, message: "delete task is succeed"}));
        })
        .catch((e) => {
          reject(res.status(400).json({status: 400, message: 'delete a task failed'}));
        });
    }
    return reject(res.status(400).json({status: 400, message: "No item to delete"}));
  });
});

/* For get a task */
taskRouter.get('/taskid/:taskId', (req, res) => {
  return new Promise((resolve, reject) => {
    if (!req.params) {
      return reject(res.status(400).send('there is no params data when get a task'));
    }
    const id = req.params.taskId;

    const taskList =  tasks.filter((item) => item.id === id);

    if (taskList.length > 0) {
      return resolve(res.json({status: 200, data: taskList[0], message: "get task is succeed"}));
    }
    return reject(res.status(400).json({status: 400, message: "get task is failed"}));
  });
});

/* For get all task */
taskRouter.get('/', (req, res) => {
  return new Promise((resolve, reject) => {
    return resolve(res.json({status: 200, data: tasks, message: "create task is succeed"}));
  });
});
module.exports = taskRouter;
