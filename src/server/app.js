const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const taskRouter = require('./Routes/taskRouter');

const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/task', taskRouter);



app.listen(port, () => {
  console.log('the api server is running on ', port);
});